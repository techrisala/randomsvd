import numpy as np
from copy import deepcopy

# Script contains Housholder tranformation routines for bidiagonalization of reactangular matrix of
# dimensions m x n; m >= n

class HouseHolder:

    @staticmethod
    def get_hh_vector(input_matrix, marker, mode):
        """
        Calculates the appropriate length HH vector (using the marker) from the given matrix
        :param input_matrix: Matrix from which HH vector will be calculated
        :param marker: Determines the length of the HH Vector
        :param mode: Premultiply or Postmultiply
        :return: HH Vector
        """

        # Asserting mode
        assert mode in ["pre", "post"], "Mode can be either 'pre' or 'post'. Your input is %r" % mode

        row_marker = marker
        col_marker = marker

        if mode == "pre":
            # Initiating HH Vector
            hh_vec = deepcopy(input_matrix[row_marker:, col_marker])
        elif mode == "post":
            # Asserting marker
            assert marker < input_matrix.shape[1]-1, "Marker should be < Index of second last row, " \
                                                     "%r" % (input_matrix.shape[0] - 2)
            # Initiating HH Vector
            hh_vec = deepcopy(input_matrix[row_marker-1, col_marker:])

        hh_vec[0] = hh_vec[0] + np.copysign(np.linalg.norm(hh_vec), hh_vec[0])  # Keeping in mind subtract. cancell.
        return hh_vec

    @staticmethod
    def householder_matmul_zero(other_matrix, hh_vector_v, mode):
        """
        Zeroes out the approporaite columns of the input matrix depending on the mode and HH_vector
        :param other_matrix: Matrix whose columns have to be zeroed out
        :param hh_vector_v: HH Vector that defines the HH Transformation
        :param mode: Premultiply or Postmultiply
        :return: Updated matrix after HH transformation has been applied
        """

        # Asserting mode
        assert mode in ["pre", "post"], "Mode can be either 'pre' or 'post'. Your input is %r" % mode

        # Getting matrix dimensions
        mat_dims = other_matrix.shape
        vec_len = hh_vector_v.shape[0]

        # Aplying HH Multiplications
        if np.dot(hh_vector_v, hh_vector_v) < 10*np.finfo(float).eps:  # If the vector is already sufficiently small
            return other_matrix
        else:
            alpha = 2 / np.dot(hh_vector_v, hh_vector_v)

        # Mode conditional
        if mode == "pre":
            marker = mat_dims[0] - vec_len
            for i in range(marker, mat_dims[1]):
                beta = alpha * np.dot(hh_vector_v, other_matrix[marker:, i])
                other_matrix[marker:, i] = other_matrix[marker:, i] - (beta * hh_vector_v)
        elif mode == "post":
            marker = mat_dims[1] - vec_len
            for i in range(marker-1, mat_dims[0]):
                beta = alpha * np.dot(hh_vector_v, other_matrix[i, marker:])
                other_matrix[i, marker:] = other_matrix[i, marker:] - (beta * hh_vector_v)

        return other_matrix

    @staticmethod
    def accumulate_hh_matrices(accumulated_matrix, new_hh_vector, mode):
        """
        Accumulate HH matrices via pre and post multiplication
        :param accumulated_matrix: Matrix which stores the accumulated product
        :param new_hh_vector: New HH vector whose HH matrix need to be accumulated via matrix mult.
        :param mode: Premultiply or Postmultiply
        :return: aacumulated matrix
        """
        # Asserting mode
        assert mode in ["pre", "post"], "Mode can be either 'pre' or 'post'. Your input is %r" % mode

        # Getting matrix dimensions
        mat_dims = accumulated_matrix.shape
        vec_len = new_hh_vector.shape[0]

        # Asserting matrix dims
        assert mat_dims[0] == mat_dims[1], "Accumulating matrix should identity and hence, square. " \
                                           "Current shape is %r" % mat_dims

        # Aplying HH Multiplications
        if np.dot(new_hh_vector, new_hh_vector) < 10*np.finfo(float).eps:  # If the vector is already sufficiently small
            return accumulated_matrix
        else:
            alpha = 2 / np.dot(new_hh_vector, new_hh_vector)

        # Marker
        marker = mat_dims[0] - vec_len

        # Mode conditional
        if mode == "pre":
            for i in range(mat_dims[1]):
                beta = alpha * np.dot(new_hh_vector, accumulated_matrix[marker:, i])
                accumulated_matrix[marker:, i] = accumulated_matrix[marker:, i] - (beta * new_hh_vector)
        elif mode == "post":
            for i in range(mat_dims[0]):
                beta = alpha * np.dot(new_hh_vector, accumulated_matrix[i, marker:])
                accumulated_matrix[i, marker:] = accumulated_matrix[i, marker:] - (beta * new_hh_vector)

        return accumulated_matrix

    @staticmethod
    def bidiagonalize(input_matrix):

        # Matrix Dimensions
        mat_dims = input_matrix.shape

        # Assert lean matrix
        assert mat_dims[0] >= mat_dims[1], "Input Matrix should be lean or square; rows >= columns"

        # A = UBV
        U = np.eye(mat_dims[0])
        V = np.eye(mat_dims[1])

        # Bidigiagonalization begins
        for i in range(mat_dims[1]):  # upto and including the last column
            hh_vec = HouseHolder.get_hh_vector(input_matrix, i, "pre")
            input_matrix = HouseHolder.householder_matmul_zero(input_matrix, hh_vec, "pre")
            U = HouseHolder.accumulate_hh_matrices(U, hh_vec, "post")
            if i < mat_dims[1]-2:
                hh_vec = HouseHolder.get_hh_vector(input_matrix, i+1, "post")
                input_matrix = HouseHolder.householder_matmul_zero(input_matrix, hh_vec, "post")
                V = HouseHolder.accumulate_hh_matrices(V, hh_vec, "pre")

        return [U, input_matrix, V]


# Checking Bidiagonalization
# B = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]], dtype='float64')
# C = np.random.rand(7, 3)
# print(C)
# print("----\n")
# out = HouseHolder.bidiagonalize(C)
# print(out[0].T @ out[0])
# print("----\n")
# print(out[2].T @ out[2])
# print("----\n")
# print(out[1])
# print("----\n")
# print(out[0] @ out[1] @ out[2])
