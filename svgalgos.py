import numpy as np
import givens as gv
import householder as hh
import svdutils as ut
import time

# Script that contains all the implementations of SVD

class SVDAlgos:

    @staticmethod
    def golubreinsch(input_matrix):

        # Stores norm of offdiagonal entries
        # to observerve convergence pattern
        # norm_store = []

        # Matrix dims
        m = input_matrix.shape[0]
        n = input_matrix.shape[1]

        # Assert lean matrix
        assert m >= n, "Input Matrix should be lean or square; rows >= columns"

        # Bidiagonalize input matrix
        [U, B, V_t] = hh.HouseHolder.bidiagonalize(input_matrix)

        # Initialzing markers which decouple B[0:n, 0:n] to B11, B22, B33 where B is m x n; m >= n
        p = 0
        q = 0

        # Iterations to tranform upper-bidiagonal matrix to diagonal matrix
        while q != n:

            # Checking whether super-diagonal elements are sufficiently small
            B[0:n, 0:n] = ut.SVDUtils.update_super_diag(B[0:n, 0:n])

            # Get the markers that decouple B[0:n, 0:n]
            p, q = ut.SVDUtils.decouple_bidiag_matrix(B[0:n, 0:n])

            # If complete diagonalization is achieved
            if q == n:
                break

            # Zeroing out rows of B if the diagonal element of that row is 0
            U[:, 0:n], B[0:n, 0:n], diag_is_zero = gv.Givens.zero_out_row(B[0:n, 0:n], p, q, U[:, 0:n])

            if not diag_is_zero:
                U[:, 0:n], B[0:n, 0:n], V_t = gv.Givens.golub_kahan_step(U[:, 0:n], B[0:n, 0:n], V_t, p, q)

            # Storing norm of upper diagonal for convergence check
            # norm_store.append(np.linalg.norm(np.diag(B[0:n, 0:n], 1)))

        # Once the matrices are formed, apply signed Identity matrix so that singular values are all non-negative
        sign_mat = ut.SVDUtils.sign_sigma(B[0:n, 0:n])
        B[0:n, 0:n] = B[0:n, 0:n] @ sign_mat
        V_t = sign_mat @ V_t

        # Once the matrices are formed, apply permuations so that B contains singular values in sorted order
        per_mat = ut.SVDUtils.sort_sigma(B[0:n, 0:n])
        B[0:n, 0:n] = per_mat @ B[0:n, 0:n] @ per_mat.T

        # Accumulating the above transformations
        U[:, 0:n] = U[:, 0:n] @ per_mat.T
        V_t = per_mat @ V_t

        return U, B, V_t


# Checking golubreinsch
# np.set_printoptions(precision=2)
# my_state = np.random.RandomState(1)
# A = my_state.rand(4, 4)
# A = np.array([
#  [0.86,  0.99,  0.97,  0.94, 0.33],
#  [0.25,  0.64,  0.35,  0.11, 0.37],
#  [0.10,  0.75,  0.26,  0.02, 0.84],
#  [0.80,  0.24,  0.45,  0.52, 0.55],
#  [0.87,  0.40,  0.19,  0.57, 0.11]
# ])
# print("--A--\n")
# print(A)
# print("----\n")
# t_start = time.time()
# U_L, Sigma, V_T, norm_stored = SVDAlgos.golubreinsch(A)
# t_end = time.time()
# print("--Time Taken--\n")
# print(t_end - t_start)
# print("----\n")
# print("--OffDiagonal Entries--\n")
# print(np.diag(Sigma, 1))
# print("----\n")
# print("--Sigma Matrix--\n")
# print(Sigma)
# print("----\n")
# print("--U.T @ U--\n")
# print(U_L.T @ U_L)
# print("----\n")
# print("--V_T.T @ V_T--\n")
# print(V_T.T @ V_T)
# print("----\n")
# print("--U @ Sigma @ V_T--\n")
# print(U_L @ Sigma @ V_T)
# print("----\n")
