import numpy as np

# Script contains utils functions for Golub-Reinsch SVD

class SVDUtils:

    @staticmethod
    def update_super_diag(input_matrix, tol=10*np.finfo(float).eps):
        """
        Zeroes out super-diagonal elements which are less than the set tolerance.
        Input matrix is bidiagonal, square.
        :param input_matrix: Matrix whose super-diagonal elements need to be compared with the set tolerance
        :param tol: A multiple of this values is used as the tolerance
        :return: Updated input_matrix after sufficiently small super-diagonal elements have been zeroed out
        """

        # Checking dimension of the matrix
        dim_matrix = input_matrix.shape
        assert dim_matrix[0] == dim_matrix[1], "Input Matrix must be square. " \
                                               "Current dimensions: %r" % dim_matrix

        # Checking if the superdiagonal values are sufficiently small
        for i in range(dim_matrix[0]-1):  # Running upto and including the second last column of input_matrix
            # Reference for tolerance value used: GVL4; Page 491
            if abs(input_matrix[i, i+1]) <= tol * (abs(input_matrix[i, i]) + abs(input_matrix[i + 1, i + 1])):
                input_matrix[i, i+1] = 0.0

        return input_matrix

    @staticmethod
    def decouple_bidiag_matrix(input_matrix):
        """
        Gives the decoupling boundaries of the given bidiagonal, square matrix into 3 matrices;
        such that input_Matrix = B we have have the markers that marks the boundaries
        between B11, B22, B33, where, B can be seen as
        B11 000 000
        000 B22 000
        000 000 B33; matrices B11 and B33 are diagonal; B22 is bidiagonal
        :param input_matrix: Matrix whose decoupling boundaries need to be figured out
        :return: boundary markers, p and q where B11 is p x p and and B33 is q x q
        p, smallest possible such that B22 has a non-zero as the first superdiagonal element.
        q, largest possible such that B33 is diagonal
        """

        # Checking dimension of the matrix
        dim_matrix = input_matrix.shape
        assert dim_matrix[0] == dim_matrix[1], "Input Matrix must be square. " \
                                               "Current dimensions: %r" % dim_matrix

        # Initializing p, q
        p = 0  # If no super-diagonal exists
        q = dim_matrix[0]  # If no super-diagonal exists

        # Searching for q
        for i in range(dim_matrix[1]-1, 0, -1):
            if input_matrix[i-1, i] != 0:
                q = dim_matrix[0] - i - 1
                break

        # Searching for p
        for i in range(1, dim_matrix[0] - q):
            if input_matrix[i-1, i] != 0:
                p = i - 1
                break

        return p, q

    @staticmethod
    def sign_sigma(sigma_matrix):
        """
        Function to turn diagonal of the sigma matrix non-negative
        :param sigma_matrix: Input Sigma (diagonal matrix)
        :return: return sign_mat (or S) such that Sigma @ S has a non-negative diagonal
        """

        # Handling Signs
        diag = np.diag(sigma_matrix)
        diag_sign = np.sign(diag)
        diag_sign[diag_sign == 0] = 1
        sign_mat = np.diag(diag_sign)

        return sign_mat

    @staticmethod
    def sort_sigma(sigma_matrix):
        """
        Function to sort the sigma matrix for the SVD
        :param sigma_matrix: Input Sigma (diagonal matrix)
        :return Permutation perm_mat( or P) such that P @ Sigma @ P.T has sorted diagonal elements;
        """

        # Handling Sorting
        diag = np.diag(sigma_matrix)
        id_matrix = np.eye(len(diag))
        vec = []
        sorted_index = np.argsort(diag)[::-1]
        for i in sorted_index:
            vec.append(id_matrix[:, i])

        perm_mat = np.vstack(vec)
        return perm_mat


# Checking decouple_bidiag_matrix
# B = np.array([[1, 0, 0, 0], [0, 2, 3, 0], [0, 0, 3, 0], [0, 0, 0, 0]])
# p_i, q_i = decouple_bidiag_matrix(B)
# print(p_i, q_i)
