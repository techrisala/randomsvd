import numpy as np

# Script contains Givens tranformation routines for Golub Kahan Step

class Givens:

    @staticmethod
    def golub_get_cos_sin(a, b):
        """
        Gives the cos(theta) and sin(theta) values corresponding to given (a, b)
        such that a couterclockwise rotation by [c -s; s c] of [a; b] turns it
        to [sqrt(a^2 + b^2); 0].
        :param a: first component of the vector which you want to rotate
        :param b: second component of the vector which you want to rotate
        :return: cos and sin theta that rotates [a; b] to obtain [sqrt(a^2 + b^2); 0]
        """
        if b == 0:
            c = 1
            s = 0
        else:
            if abs(b) > abs(a):
                tau = -a / b
                s = 1 / (np.sqrt(1 + tau ** 2))  # To prevent overflow/underflow
                c = s * tau
            else:
                tau = -b / a
                c = 1 / (np.sqrt(1 + tau ** 2))  # To prevent overflow/underflow
                s = c * tau
        return c, s

    def get_cos_sin(x, y):
        """
        Gives the cos(theta) and sin(theta) values corresponding to given (a, b)
        such that a couterclockwise rotation by [c -s; s c] of [a; b] turns it
        to [sqrt(a^2 + b^2); 0].
        :param x: first component of the vector which you want to rotate
        :param y: second component of the vector which you want to rotate
        :return: cos and sin theta that rotates [a; b] to obtain [sqrt(a^2 + b^2); 0]
        """
        if y == 0:  # No rotation necessary
            c = 1
            s = 0
        else:
            if abs(y) > abs(x):
                tau = -x / y
                s = -1 / (np.sqrt(1 + tau ** 2))  # To prevent overflow/underflow
                c = s * tau
            else:
                tau = -y / x
                c = 1 / (np.sqrt(1 + tau ** 2))  # To prevent overflow/underflow
                s = c * tau
        return c, s

    @staticmethod
    def zero_out_row(input_matrix, p, q, input_u, tol=10*np.finfo(float).eps):
        """
        Zeros out the entire row using Givens rotation if the diagonal element of that
        row is 0. Input matrix is square, bidiagonal.
        :param input_matrix: Whole input matrix
        :param p: Top Marker for the portion of input matrix of our interest
        :param q: Bottom Marker for the portion of input matrix of our interest
        :param input_u: Matrix where the Givens transformations need to be accumulated
        :param tol: A factor of this tolerance will be used to determine whether the diagonal element
        is small enough
        :return: The updated input_matrix (after possible zeroing out of appropriate rows) and updated
        orthogonal which also accumulates the applied Givens rotation. It also returns an boolean
        indicating whether the input_matrix has a zero diagonal entry
        """

        # Assert square matrix
        assert input_matrix.shape[0] == input_matrix.shape[1], "Input Matrix must be square."

        # Boolean which indicates whether there exist at least 1 zero diagonal entry in input_matrix
        diag_is_zero = False

        n = input_matrix.shape[1]
        for i in range(p, n-q-1):
            if abs(input_matrix[i, i]) <= tol * np.linalg.norm(input_matrix, ord='fro'):  # Diagonal element tolerance
                # Reference for tolerance value: GVL4, Page 491
                input_matrix[i, i] = 0.0
                diag_is_zero = True
                for j in range(i, n-1):
                    c, s = Givens.get_cos_sin(input_matrix[i, j+1], input_matrix[j+1, j+1])
                    givens_pre = np.array([[s, c], [c, -s]])
                    # Rotation transformation
                    input_matrix[[i, j+1], j+1:] = givens_pre @ input_matrix[[i, j+1], j+1:]
                    # Accumulating givens
                    input_u[:, [i, j+1]] = input_u[:, [i, j+1]] @ givens_pre.T

        return input_u, input_matrix, diag_is_zero

    @staticmethod
    def golub_kahan_step(input_u, input_matrix, input_v, p, q):
        """
        Performs Golub Kahan step over the input bidiagonal matrix matrix such that the updated
        input_matrix has smaller superdiagonal elements. Input matrix is square, bidiagonal.
        :param input_u: Orthogonal matrix accumulating transpose pre-Givens rotations
        :param input_matrix: Input matrix over which Golub Kahan Step needs to be performed
        :param input_v: Orthogonal matrix accumulating transpose of post-Givens rotations
        :param p: Top Marker for the portion of input matrix of our interest
        :param q: Bottom Marker for the portion of input matrix of our interest
        :return: Updated input_u, Updated input_matrix, Updated input_v
        """

        # Assert square matrix
        assert input_matrix.shape[0] == input_matrix.shape[1], "Input Matrix must be square."

        # Calculating eigen values of the trailing matrix of (input_matrix.T * input_matrix) ==> it's tridiagonal
        trailing_B = input_matrix[-2:, -2:].T @ input_matrix[-2:, -2:]
        eig_val, eig_vec = np.linalg.eig(trailing_B)
        # Wilkinson Shift
        mu = eig_val[0] if abs(eig_val[0] - trailing_B[1, 1]) <= abs(eig_val[1] - trailing_B[1, 1]) else eig_val[1]
        y = (input_matrix[p, p]**2) - mu
        z = input_matrix[p, p]*input_matrix[p, p+1]
        # Moving down the bidiagonal matrix by applying pre and post Givens Rotations
        for k in range(p, input_matrix.shape[0] - q - 1):
            # Applying post Givens
            c, s = Givens.get_cos_sin(y, z)
            given_post = np.array([[c, s], [-s, c]])
            input_matrix[max(p, k-1):k+3, k:k+2] = input_matrix[max(p, k-1):k+3, k:k+2] @ given_post
            # Accumulating Post Givens.T
            input_v[k:k+2, :] = given_post.T @ input_v[k:k+2, :]
            # Appplying pre Givens
            y = input_matrix[k, k]
            z = input_matrix[k+1, k]
            c, s = Givens.get_cos_sin(y, z)
            given_pre = np.array([[c, -s], [s, c]])
            input_matrix[k:k+2, k:k+3] = given_pre @ input_matrix[k:k+2, k:k+3]
            # Accumulating pre Givens.T
            input_u[:, k:k+2] = input_u[:, k:k+2] @ given_pre.T
            # Iterating till the second last column of the selected row of B22
            if k < input_matrix.shape[1] - q - 2:
                y = input_matrix[k, k+1]
                z = input_matrix[k, k+2]
        return input_u, input_matrix, input_v


# Checking zero_out_row and Golub Kahan Step
# np.set_printoptions(precision=2)
# B = np.array([
#     [1, 3, 00, 00, 00, 0],
#     [0, 0, 9, 00, 00, 0],
#     [0, 0, 00, 91, 00, 0],
#     [0, 0, 00, 41, 23, 0],
#     [0, 0, 00, 00, 53, 65],
#     [0, 0, 00, 00, 00, 12]
# ], dtype='float64')
# input_U = np.eye(6)
# input_V = np.eye(6)
# print(Givens.zero_out_row(B, 1, 1, input_U)[1])
# for i in range(30):
#     Givens.golub_kahan_step(input_U, B, input_V, 0, 0)
#     print((np.linalg.norm(np.linalg.diag(B, 1))))
# print(np.linalg.matrix_rank(np.random.rand(1000, 100)))
# print(np.linalg.matrix_rank(B))

# Checking for Givens which resulting in [0; *] where * < 0
# a = 20.969880696
# b = 53.0
# c, s = Givens.get_cos_sin(a, b)
# print(c, s)
# givens_pre = np.array([
#     [c, -s],
#     [s, c]
# ])
# vec = np.array([a, b])
# print(givens_pre @ vec)
