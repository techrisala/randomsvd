import numpy as np
import imageio as im
import svgalgos as golub
import time

def compress_image(image_url, k):

    # Read Image
    read_image = np.array(im.imread(image_url), dtype="float64")

    # Time it
    t_start = time.time()

    # Making the matrix lean if needed
    not_lean = False
    not_3_layers = False
    if read_image.shape[0] < read_image.shape[1]:
        not_lean = True

    try:
        limit = read_image.shape[2]
        if not_lean:
            read_image = np.transpose(read_image, [1, 0, 2])
        for i in range(limit):
            read_image[:, :, i] = self_general_svd(read_image[:, :, i], k)
            print("Done")
    except IndexError:
        not_3_layers = False
        if not_lean:
            read_image = read_image.T
        read_image[:, :] = self_general_svd(read_image[:, :], k)
        print("Done")

    # Time it
    t_end = time.time()
    print(t_end - t_start)

    # Write the compresed image
    if not_lean and not_3_layers:
        read_image = read_image.T
    elif not_lean and not not_3_layers:
        read_image = np.transpose(read_image, [1, 0, 2])

    im.imwrite("./sample_images/threespheres_out.jpg", np.uint8(read_image))

    return None

def benchmark_random_svd(input_image, k):

    # Read image into matrix
    (m, n) = input_image.shape  # dim: m x n

    # Generating random Matrix
    random_matrix = np.random.rand(n, k)  # dim: n x k

    # Computing sampled matrix
    sampled_A = np.matmul(input_image, random_matrix)  # dim: m x k

    # Getting orthonormal basis of smapled_A via reduced QR factorization
    [Q, R] = np.linalg.qr(sampled_A, 'reduced')  # Q dim: m x k

    # Form matrix B
    B = np.matmul(np.transpose(Q), input_image)  # dim: k x n

    # Computing thin SVD of smaller matrix B
    [U_hat, Sigma, V_t] = np.linalg.svd(B, False)

    # Forming final U
    U = np.matmul(Q, U_hat)

    # Compressed input_image
    comp_A = np.uint8(np.matmul(np.matmul(U, np.diag(Sigma)), V_t))

    return comp_A

def benchmark_general_svd(input_image, k):

    # Computing SVD of the entire matrix
    [U, Sigma, V_t] = np.linalg.svd(input_image, True)

    # Compressed input_image
    comp_A = np.uint8(U[:, 0:k] @ np.diag(Sigma[0:k]) @ V_t[0:k, :])

    return comp_A

def self_random_svd(input_image, k):
    # Read image into matrix
    (m, n) = input_image.shape  # dim: m x n

    # Generating random Matrix
    random_matrix = np.random.rand(n, k)  # dim: n x k

    # Computing sampled matrix
    sampled_A = np.matmul(input_image, random_matrix)  # dim: m x k

    # Getting orthonormal basis of smapled_A via reduced QR factorization
    [Q, R] = np.linalg.qr(sampled_A, 'reduced')  # Q dim: m x k

    # Form matrix B
    B = np.matmul(np.transpose(Q), input_image)  # dim: k x n

    # Computing thin SVD of smaller matrix B
    [U_hat, Sigma, V_t] = golub.SVDAlgos.golubreinsch(B)

    # Forming final U
    U = np.matmul(Q, U_hat)

    # Compressed input_image
    comp_A = np.uint8(np.matmul(np.matmul(U, Sigma), V_t))

    return comp_A

def self_general_svd(input_image, k):

    # Computing SVD of the entire matrix
    U, Sigma, V_t = golub.SVDAlgos.golubreinsch(input_image)
    comp_A = U[:, 0:k] @ Sigma[0:k, 0:k] @ V_t[0:k, :]

    return comp_A


# compress_image("./sample_images/buzz.gif", 90)
compress_image("./sample_images/threespheres.jpg", 50)
